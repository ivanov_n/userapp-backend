# User App

## About

A simple Spring Boot application to manage user accounts.

## How to test

### Prerequisites

- Install [Git](https://git-scm.com/)
- Install [Maven](https://maven.apache.org/)
- Install [Docker](https://docs.docker.com/get-docker/)

### Running

Clone the project

```sh
$ git clone https://bitbucket.org/ivanov_n/userapp-backend.git
```

From the project's root directory execute the following commands to build the Spring application and create a docker image with the generated jar file:

```sh
$ mvn clean package
$ docker build -t usarapp:latest .
```

Create a shared network for the Spring application and the MySQL server
```sh
$ docker network create userapp_network
```
Start the MySql server
```sh
$ docker run -it --name userapp_db --network userapp_network -e MYSQL_ROOT_PASSWORD=root123 -d percona
```

Start the Spring server

```sh
$ docker run -d -it -p 8080:8080 --network userapp_network --name userapp_server usarapp:latest
```
Note that you might need to use a different port on the host system if 8080 is already taken. For example, in order to use port 9090, replace `8080:8080` with `9090:8080`.

Navigate to [http://localhost:8080/user-accounts](http://localhost:8080/user-accounts) (Replace 8080 with the port you specified in the previous command), or import and use the [userapp.postman_collection.json](userapp.postman_collection.json) Postman collection under the project's root directory.


