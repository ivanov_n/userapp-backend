package eu.nivanov.demo.userapp.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(Long id, String type) {
        super(type + " with id " + id + " doesn't exist");
    }
}
