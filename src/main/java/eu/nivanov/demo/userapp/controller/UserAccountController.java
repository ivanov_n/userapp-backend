package eu.nivanov.demo.userapp.controller;

import eu.nivanov.demo.userapp.model.dto.UpsertUserAccountTO;
import eu.nivanov.demo.userapp.model.dto.UserAccountTO;
import eu.nivanov.demo.userapp.service.useraccount.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping(value = "/user-accounts", produces = {"application/json"})
public class UserAccountController {

    private final UserAccountService userAccountService;

    @Autowired
    public UserAccountController(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @GetMapping
    public Collection<UserAccountTO> getAccounts(@RequestParam(value = "orderBy", required = false) String fieldName,
                                                 @RequestParam(value = "direction", required = false, defaultValue = "ASC") Sort.Direction direction) {
        Sort sort = fieldName != null ? Sort.by(direction, fieldName) : null;
        return userAccountService.listAccounts(sort);
    }

    @PostMapping(consumes = {"application/json"})
    public UserAccountTO createAccount(@Valid @RequestBody UpsertUserAccountTO createAccountTO) {
        return userAccountService.createAccount(createAccountTO);
    }

    @PutMapping(value = "/{id}", consumes = {"application/json"})
    public UserAccountTO updateAccount(@PathVariable("id") Long id, @Valid @RequestBody UpsertUserAccountTO userAccountTO) {
        UserAccountTO account = UserAccountTO.of(id, userAccountTO);
        return userAccountService.updateAccount(account);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        userAccountService.deleteById(id);
    }
}
