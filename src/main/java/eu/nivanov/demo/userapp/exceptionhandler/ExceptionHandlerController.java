package eu.nivanov.demo.userapp.exceptionhandler;

import eu.nivanov.demo.userapp.exception.ResourceNotFoundException;
import eu.nivanov.demo.userapp.model.dto.ErrorTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    /**
     * Handles exceptions thrown when validation of a field fails
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

        List<String> errors = new ArrayList<>();
        fieldErrors.forEach(fieldError -> errors.add(fieldError.getField() + ": " + fieldError.getDefaultMessage()));

        log.warn("MethodArgumentNotValidException caught. Errors: {}", errors);
        return createErrorResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, errors);
    }

    /**
     * Handles exceptions thrown when a parameter conversion fails
     */
    @ExceptionHandler(value = {ConversionFailedException.class})
    public ResponseEntity<Object> onConversionFailedException(ConversionFailedException exception, WebRequest req) {
        log.warn("ConversionFailedException caught. Message: {}", exception.getMessage());
        return createErrorResponseEntity(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    /**
     * Handles custom exceptions thrown when a resource doesn't exist
     */
    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity<Object> onResourceNotFoundException(ResourceNotFoundException exception, WebRequest req) {
        log.warn("ResourceNotFoundException caught. Message: {}", exception.getMessage());
        return createErrorResponseEntity(HttpStatus.NOT_FOUND, exception.getMessage());
    }

    /**
     * Handles exceptions not caught by the above methods
     */
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> onExceptionThrown(Exception exception, WebRequest req) {
        log.error("An unexpected exception caught", exception);
        return createErrorResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, "Oops, something went wrong");
    }


    private ResponseEntity<Object> createErrorResponseEntity(HttpStatus httpStatus, List<String> errors) {
        return new ResponseEntity<>(
                new ErrorTo(httpStatus, errors),
                httpStatus);
    }


    private ResponseEntity<Object> createErrorResponseEntity(HttpStatus httpStatus, String error) {
        return new ResponseEntity<>(
                new ErrorTo(httpStatus, error),
                httpStatus);

    }
}
