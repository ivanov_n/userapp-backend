package eu.nivanov.demo.userapp.repository;

import eu.nivanov.demo.userapp.model.ddo.UserAccount;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, Long> {

    @Override
    List<UserAccount> findAll();

    @Override
    List<UserAccount> findAll(Sort sort);
}
