package eu.nivanov.demo.userapp.model.dto;


import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE) // Required by the builder
@EqualsAndHashCode
public class UserAccountTO {

    @NotNull
    private Long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @Email
    private String email;

    @NotNull
    private LocalDate birthDate;

    public static UserAccountTO of(long id, @NonNull UpsertUserAccountTO source) {
        return UserAccountTO
                .builder()
                .id(id)
                .firstName(source.getFirstName())
                .lastName(source.getLastName())
                .email(source.getEmail())
                .birthDate(source.getBirthDate())
                .build();
    }
}
