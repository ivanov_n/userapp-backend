package eu.nivanov.demo.userapp.model.dto;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ErrorTo {

    private int statusCode;
    private List<String> errors;

    public ErrorTo(HttpStatus status, List<String> errors) {
        this.statusCode = status.value();
        this.errors = errors;
    }

    public ErrorTo(HttpStatus status, String error) {
        this.statusCode = status.value();
        this.errors = new ArrayList<>(1);
        errors.add(error);
    }
}
