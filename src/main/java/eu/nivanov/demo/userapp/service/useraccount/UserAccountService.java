package eu.nivanov.demo.userapp.service.useraccount;

import eu.nivanov.demo.userapp.exception.ResourceNotFoundException;
import eu.nivanov.demo.userapp.model.dto.UpsertUserAccountTO;
import eu.nivanov.demo.userapp.model.dto.UserAccountTO;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;

import java.util.Collection;

public interface UserAccountService {

    /**
     * Creates a new account from the provided data.
     */
    UserAccountTO createAccount(UpsertUserAccountTO createUserAccountTO);

    /**
     * Lists all accounts optionally sorting them
     */
    Collection<UserAccountTO> listAccounts(@Nullable Sort sort);

    /**
     * Deletes an account by id if exists.
     *
     * @throws ResourceNotFoundException if the account doesn't exist.
     */
    void deleteById(long id);

    /**
     * Updates an account if exists.
     *
     * @throws ResourceNotFoundException if the account doesn't exist.
     */
    UserAccountTO updateAccount(UserAccountTO userAccountTO);

    /**
     * Checks if an account with the provided id exists, throwing a {@link ResourceNotFoundException} if not.
     */
    void validateExists(long id);
}
