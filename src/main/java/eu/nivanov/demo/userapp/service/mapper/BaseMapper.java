package eu.nivanov.demo.userapp.service.mapper;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Defines the basic functionality of a DDO-DTO object mapper.
 *
 * @param <DomainObject>   The type of the domain object.
 * @param <TransferObject> The type of the transfer object.
 */
public interface BaseMapper<DomainObject, TransferObject> {

    /**
     * Converts to domain object to a transfer object.
     *
     * @param domainObject The source object.
     */
    TransferObject toTO(DomainObject domainObject);

    /**
     * Converts the transfer object to a domain object.
     *
     * @param transferObject The source object.
     */
    DomainObject toDO(TransferObject transferObject);

    /**
     * Maps multiple domain objects to transfer objects.
     *
     * @param domainObjects The source objects.
     */
    default Collection<TransferObject> toTO(Collection<DomainObject> domainObjects) {
        return domainObjects
                .stream()
                .map(this::toTO)
                .collect(Collectors.toList());
    }

    /**
     * Maps multiple transfer objects to domain objects.
     *
     * @param transferObjects The source objects.
     */
    default Collection<DomainObject> toDO(Collection<TransferObject> transferObjects) {
        return transferObjects
                .stream()
                .map(this::toDO)
                .collect(Collectors.toList());
    }
}
