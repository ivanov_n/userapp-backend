package eu.nivanov.demo.userapp.service.mapper.useraccount;

import eu.nivanov.demo.userapp.model.ddo.UserAccount;
import eu.nivanov.demo.userapp.model.dto.UpsertUserAccountTO;
import eu.nivanov.demo.userapp.model.dto.UserAccountTO;
import eu.nivanov.demo.userapp.service.mapper.BaseMapper;

public interface UserAccountMapperService extends BaseMapper<UserAccount, UserAccountTO> {

    UserAccount toDO(UpsertUserAccountTO upsertUserAccountTO);

}
