package eu.nivanov.demo.userapp.service.mapper.useraccount;

import eu.nivanov.demo.userapp.model.ddo.UserAccount;
import eu.nivanov.demo.userapp.model.dto.UpsertUserAccountTO;
import eu.nivanov.demo.userapp.model.dto.UserAccountTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAccountMapperServiceImpl implements UserAccountMapperService {

    private final ModelMapper modelMapper;

    @Autowired
    public UserAccountMapperServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserAccount toDO(UpsertUserAccountTO upsertUserAccountTO) {
        return modelMapper.map(upsertUserAccountTO, UserAccount.class);
    }

    @Override
    public UserAccountTO toTO(UserAccount userAccount) {
        return modelMapper.map(userAccount, UserAccountTO.class);
    }

    @Override
    public UserAccount toDO(UserAccountTO userAccountTO) {
        return modelMapper.map(userAccountTO, UserAccount.class);
    }
}
