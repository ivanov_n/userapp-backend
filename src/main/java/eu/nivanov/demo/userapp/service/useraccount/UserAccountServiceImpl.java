package eu.nivanov.demo.userapp.service.useraccount;

import eu.nivanov.demo.userapp.exception.ResourceNotFoundException;
import eu.nivanov.demo.userapp.model.ddo.UserAccount;
import eu.nivanov.demo.userapp.model.dto.UpsertUserAccountTO;
import eu.nivanov.demo.userapp.model.dto.UserAccountTO;
import eu.nivanov.demo.userapp.repository.UserAccountRepository;
import eu.nivanov.demo.userapp.service.mapper.useraccount.UserAccountMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class UserAccountServiceImpl implements UserAccountService {

    private final UserAccountRepository userAccountRepository;

    private final UserAccountMapperService userAccountMapperService;

    @Autowired
    public UserAccountServiceImpl(UserAccountRepository userAccountRepository,
                                  UserAccountMapperService userAccountMapperService) {
        this.userAccountRepository = userAccountRepository;
        this.userAccountMapperService = userAccountMapperService;
    }

    @Override
    public UserAccountTO createAccount(UpsertUserAccountTO createUserAccountTO) {
        UserAccount accountToCreate = userAccountMapperService.toDO(createUserAccountTO);
        UserAccount persistedAccount = userAccountRepository.save(accountToCreate);
        return userAccountMapperService.toTO(persistedAccount);
    }

    @Override
    public Collection<UserAccountTO> listAccounts(Sort sort) {
        List<UserAccount> accounts = sort != null ? userAccountRepository.findAll(sort) : userAccountRepository.findAll();
        return userAccountMapperService.toTO(accounts);
    }

    @Override
    public void deleteById(long id) {
        validateExists(id);
        userAccountRepository.deleteById(id);
    }

    @Override
    public UserAccountTO updateAccount(UserAccountTO userAccountTO) {
        validateExists(userAccountTO.getId());
        UserAccount accountToCreate = userAccountMapperService.toDO(userAccountTO);
        UserAccount persistedAccount = userAccountRepository.save(accountToCreate);
        return userAccountMapperService.toTO(persistedAccount);
    }

    @Override
    public void validateExists(long id) {
        userAccountRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, "User Account"));
    }
}
