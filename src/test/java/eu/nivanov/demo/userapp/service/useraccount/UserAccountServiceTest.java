package eu.nivanov.demo.userapp.service.useraccount;

import eu.nivanov.demo.userapp.model.ddo.UserAccount;
import eu.nivanov.demo.userapp.model.dto.UpsertUserAccountTO;
import eu.nivanov.demo.userapp.model.dto.UserAccountTO;
import eu.nivanov.demo.userapp.repository.UserAccountRepository;
import eu.nivanov.demo.userapp.service.mapper.useraccount.UserAccountMapperService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SpringBootTest
public class UserAccountServiceTest {

    private static final List<UserAccount> predefinedAccounts;
    private static List<UserAccount> testSpecificAccounts;

    static {
        ArrayList<UserAccount> userAccounts = new ArrayList<>();
        userAccounts.add(
                UserAccount
                        .builder()
                        .id(1L)
                        .firstName("Tim")
                        .lastName("Smith")
                        .email("timsmith@test.eu")
                        .birthDate(LocalDate.of(1990, 5, 7))
                        .build()
        );
        userAccounts.add(
                UserAccount
                        .builder()
                        .id(2L)
                        .firstName("Sara")
                        .lastName("Smith")
                        .email("sarasmith@test.eu")
                        .birthDate(LocalDate.of(1992, 7, 8))
                        .build()
        );
        predefinedAccounts = Collections.unmodifiableList(userAccounts);
    }

    @MockBean
    private UserAccountRepository userAccountRepository;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private UserAccountMapperService userAccountMapperService;

    @BeforeEach
    void initEnv() {
        testSpecificAccounts = new ArrayList<>(predefinedAccounts);
        Mockito.when(userAccountRepository.findAll()).thenReturn(testSpecificAccounts);
    }

    @Test
    void testThatCanFetchAllUserAccounts() {
        // Given
        Collection<UserAccountTO> expectedUserAccountTOs = userAccountMapperService.toTO(testSpecificAccounts);

        // When
        Collection<UserAccountTO> actualUserAccountTOs = userAccountService.listAccounts(null);

        // Then
        assertSameMembers(expectedUserAccountTOs, actualUserAccountTOs, "The returned accounts aren't the expected");
    }

    @Test
    void testThatUserAccountCanBeCreated() {
        // Given
        Mockito.when(userAccountRepository.save(Mockito.any(UserAccount.class))).thenAnswer(i -> {
            UserAccount account = i.getArgument(0);
            testSpecificAccounts.add(account);
            return account;
        });
        UpsertUserAccountTO accountToCreate = newUserAccount();
        ArrayList<UserAccount> expectedUserAccounts = new ArrayList<>(predefinedAccounts);
        expectedUserAccounts.add(userAccountMapperService.toDO(accountToCreate));

        // When
        userAccountService.createAccount(accountToCreate);

        // Then
        Collection<UserAccountTO> actualUserAccountTOs = userAccountService.listAccounts(null);
        Collection<UserAccountTO> expectedUserAccountTOs = userAccountMapperService.toTO(expectedUserAccounts);
        assertSameMembers(expectedUserAccountTOs, actualUserAccountTOs, "The returned accounts aren't the expected");
    }

    private UpsertUserAccountTO newUserAccount() {
        UpsertUserAccountTO account = new UpsertUserAccountTO();
        account.setFirstName("Test");
        account.setLastName("Tester");
        account.setEmail("test@test.eu");
        account.setBirthDate(LocalDate.now());
        return account;
    }

    private void assertSameMembers(Collection<?> expected, Collection<?> actual, String message) {
        Assertions.assertEquals(expected.size(), actual.size(), message);
        Assertions.assertTrue(expected.containsAll(actual), message);
    }

    // TODO: Not a complete test suite
}
