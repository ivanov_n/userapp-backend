FROM openjdk:11
CMD ["mkdir", "app"]
WORKDIR app/
COPY target/userapp-backend.jar app/app.jar
EXPOSE 8080
CMD ["java", "-jar", "app/app.jar"]